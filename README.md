# Financial Calculator
Financial Calculator is an application for calculating the time it takes to acquire a certain amount of wealth or the time it takes to retire with a certain amount of passive income, given a certain rate of growth, continual investment and possible starting amount.
